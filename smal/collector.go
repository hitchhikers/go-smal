package smal

import (
	"fmt"
	"regexp"
	"time"

	"github.com/gocolly/colly/v2"
)

// Collector defines a new collector for storting both the
// colly.Collector as well as other options.
type Collector struct {
	collyCollector *colly.Collector
	Lecturer       string
	Course         string
	Count          int
}

// CollyConfigs is a collection of availableflags for colly
// Maybe implement as a function.  Look at official docs.
type CollyConfigs struct{}

// NewCollector inilialize a new collector for each subject.
func NewCollector(lecturer string, course string, count int) *Collector {
	return &Collector{collyCollector: colly.NewCollector(), Lecturer: lecturer, Course: course, Count: count}
}

// GetLectures retrieves lectures.
func (c *Collector) GetLectures() ([]Lecture, error) {
	lectureQuery := "#lectures > tbody > tr.lecture"

	var pageCounter *int
	pageCounter = new(int)

	if c.Count == 0 {
		return nil, nil
	}

	if c.Count == -1 {
		// TODO:
		*pageCounter = -1
	} else {
		*pageCounter = 1 + (c.Count / lecturesPerPage)
	}

	var lectures []Lecture

	c.collyCollector.OnHTML(lectureQuery, func(e *colly.HTMLElement) {
		// get all urls and types
		urlHref := e.ChildAttrs("td:nth-child(6) a", "href")
		urlName := e.ChildAttrs("td:nth-child(6) a", "title")
		urlMap := make(map[string]string, len(urlName))

		// Get first word of title only
		word := regexp.MustCompile("\\w+")
		for i, v := range urlName {
			urlName[i] = word.FindAllString(v, -1)[0]
		}
		// Fill map with name as Key and href as Value
		for i, k := range urlName {
			urlMap[k] = defaultDomain + defaultPath + "/" + urlHref[i]
		}

		// Deserialize ID
		id := StripTrailingSlash(urlHref[0])

		tidspunkt, err := time.Parse(DateLayout, e.ChildText("td:nth-child(1)"))
		if err != nil {
			panic(err)
		}

		lecture := Lecture{
			ID:        id,
			Tidspunkt: tidspunkt,
			Foreleser: e.ChildText("td:nth-child(3)"),
			Tittel:    e.ChildText("td:nth-child(4)"),
			Emne:      e.ChildText("td:nth-child(5)"),
			URL:       urlMap,
			Info:      e.ChildAttr("td:nth-child(7) img", "title"),
		}

		lecture.DeserializeElapsedTime(e.ChildText("td:nth-child(2)"))

		lectures = append(lectures, lecture)
	})

	pageQuery := ".paginator:nth-child(1) > p"

	c.collyCollector.OnHTML(pageQuery, func(e *colly.HTMLElement) {
		t := e.ChildAttr("a:contains(\"Neste\")", "href")

		// Continues if only when more pages to scrape.
		if *pageCounter > 0 {
			if t != "" {
				c.collyCollector.Visit(defaultDomain + t)
			}
		}
	})

	// Before making a request print "Visiting ..."
	// TODO: Denne trengs ikke!
	c.collyCollector.OnRequest(func(r *colly.Request) {
		if VerboseEnabled {
			fmt.Println("Visiting", r.URL.String())
		}
	})

	c.collyCollector.OnResponse(func(r *colly.Response) {
		// Decrement counter when a response is given.
		*pageCounter--
	})

	// NB! Need here to replace the space with a "+" symbol.
	// WARNING! Written `topic` and not `subject` below!
	c.collyCollector.Visit(defaultDomain +
		defaultPath +
		"/index.php?lecturer=" +
		ReplaceSpaceWithChar(c.Lecturer, "+") +
		"&topic=" +
		ReplaceSpaceWithChar(c.Course, "+"))

	return lectures, nil
}

// CollectLectures get all the subjects given by the map.
func CollectLectures(config *Config) (lectures [][]Lecture, err error) {
	lectures = make([][]Lecture, len(config.Queries))

	// TODO: Fill body instead
	for idx, q := range config.Queries {
		c := NewCollector(q.Lecturer, q.Course, q.Count)
		lectures[idx], err = c.GetLectures()

		if err != nil {
			return nil, err
		}
	}

	return lectures, nil
}
