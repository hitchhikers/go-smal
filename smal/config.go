package smal

import (
	// Used for Sprintf in reading file

	"fmt"
	"io/ioutil"
	"os"      // Used for getting enviornment variables
	"strings" // Used for join

	"github.com/pelletier/go-toml"
)

// ConfigLocations to potential config locations to check
var ConfigLocations = []string{
	strings.Join([]string{GetEnv("XDG_CONFIG_HOME"), "smal"}, "/"),
	GetEnv("XDG_CONFIG_HOME"),
	".",
}

// Config contains variables used for configuration.
// Member data are set as private to avoid changing them at runtime.
type Config struct {
	Debug   bool
	OutDir  string `toml:"out_dir"`
	Queries []Query
}

// Query hold the metadata for each query.
type Query struct {
	Lecturer string
	Course   string `toml:"course"`
	Body     []Lecture
	Type     []string
	Count    int `default:"-2"` // -2 means not set, -1 all.
}

// ReadConfig parses config files and overwrite existing data
// Takes path to a toml file and parses it's content
// Returns an error if anything fails.
func (c *Config) ReadConfig(filePath string) (err error) {
	// Read the file
	data, err := ioutil.ReadFile(filePath)

	if err != nil {
		return err
	}

	// Unmarshal the .toml file
	err = toml.Unmarshal([]byte(data), c)

	if err != nil {
		return err
	}

	if c.OutDir == "" {
		c.OutDir, err = findDl()
		if err != nil {
			fmt.Println(err)
		}
	}

	for idx := range c.Queries {
		if c.Queries[idx].Lecturer == "" && c.Queries[idx].Course == "" {
			// TODO: Support for custom filename in error
			// message.
			panic("Could not read the .toml file as it contains syntax error!")
		}

		if c.Queries[idx].Lecturer == "" {
			c.Queries[idx].Lecturer = "all"
		}

		if c.Queries[idx].Course == "" {
			c.Queries[idx].Course = "all"
		}

		if c.Queries[idx].Count == -2 {
			c.Queries[idx].Count = defaultDownloadCount
		}
	}

	return nil
}

// FindConfig checks to see if a path contains a file named ConfigName
// Takes a string slice of arrays
// Returns path/ConfigName if the file exist
func FindConfig(paths []string) (path string, err error) {
	for _, path := range paths {
		path = strings.Join([]string{path, ConfigName}, "/")
		if _, err := os.Stat(path); err == nil {
			return path, nil
		}
	}

	// Edgecase for ~/.smal.toml
	path = strings.Join([]string{GetEnv("HOME"), ConfigName}, "/.")
	if _, err := os.Stat(path); err == nil {
		return path, err
	}
	return path, err
}

// GetEnv returns the contents of an environment variable
// Takes variable name as a string
// Returns the content of the variable if it is set,
//   otherwise returns an empty string
func GetEnv(variable string) string {
	if env, ok := os.LookupEnv(variable); ok {
		return env
	}
	return ""
}

// GetDebug returns whether the config has enable debug support
func (c *Config) GetDebug() bool {
	return c.Debug
}

func findDl() (outdir string, err error) {
	outdir = "./"
	paths := []string{"Downloads", "Dl", "Nedlastinger"}
	if home, err := os.UserHomeDir(); err == nil {
		for _, path := range paths {
			path = strings.Join([]string{home, path}, "/")
			if _, err := os.Stat(path); err == nil {
				return path, nil
			}
		}
	}
	return outdir, fmt.Errorf(
		"SMAL: download directory not found or specifed in config, using %q",
		outdir,
	)
}
