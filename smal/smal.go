package smal

import (
	"regexp"
)

const (
	// Version defines a version
	Version = "0.3.1-alpha.0"

	// SmalAuthor holds the copyright holder
	SmalAuthor = "(c) 2020 - present Birger J. Nordølum"

	defaultUserAgent = "smal.go/" + Version

	// ConfigName defines the default filename for the configuration
	ConfigName = "smal.toml"

	defaultDomain = "https://forelesning.gjovik.ntnu.no"
	defaultPath   = "/publish"

	apiVersion = "v0"

	lecturesPerPage      = 100
	defaultDownloadCount = 5

	// MaxConcurrentDownloads number of concurrent downloads
	MaxConcurrentDownloads = 3

	// Banner is our small logo
	Banner = "\n" +
		"     _______.       .___  ___.           ___            __ \n" +
		"    /       |       |   \\/   |          /   \\          |  | \n" +
		"   |   (----` ______|  \\  /  |  ______ /  ^  \\   ______|  | \n " +
		"    \\   \\   |______|  |\\/|  | |______/  /_\\  \\ |______|  | \n" +
		".----)   |          |  |  |  |       /  _____  \\       |  `----. \n" +
		"|_______/           |__|  |__|      /__/     \\__\\      |_______| \n"
)

// VerboseEnabled tells if we've enabled verbose via the cli
var VerboseEnabled bool

// StripTrailingSlash removes the trailing forward slash
func StripTrailingSlash(str string) string {
	pattern := regexp.MustCompile(`^[^\/]+`)

	return pattern.FindString(str)
}

// ReplaceSpaceWithChar looks for a space and replaces it with a character of
// choice.
func ReplaceSpaceWithChar(str string, char string) string {
	re := regexp.MustCompile(`\s`)
	return re.ReplaceAllString(str, char)
}
