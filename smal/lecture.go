package smal

import (
	"fmt"
	"regexp"
	"strconv"
	"time"
)

// LectureType is the generic type for lecture formats.
type LectureType int

// Contains the enum types for possible lecture type
const (
	Audio    LectureType = iota // Audio only
	Camera   LectureType = iota // Camera only
	Combined LectureType = iota // Both camera & screen
	Screen   LectureType = iota // Only screen

	DateLayout = "2006-01-02 15:04" // Date format from site
)

// LecturesService contains the collector.
type LecturesService struct {
	collector *Collector
}

// Lecture inneholder all metadata om en forelesning.
type Lecture struct {
	ID        string
	Tidspunkt time.Time
	Varighet  int
	Foreleser string
	Tittel    string
	Emne      string
	URL       map[string]string
	Info      string
}

// DeserializeElapsedTime converts the string "number min" to number
func (l *Lecture) DeserializeElapsedTime(str string) {
	pattern := regexp.MustCompile(`\d+`)
	s := pattern.FindString(str)

	l.Varighet, _ = strconv.Atoi(s)
}

// Disp prints a human readable oupt
func (l *Lecture) Disp() {
	fmt.Printf(
		"Tittel: %s"+
			"\nVarigheit: %d minutt - Foreleser: %s"+
			"\nLenke: %v\n",
		l.Tittel,
		l.Varighet,
		l.Foreleser,
		l.URL["Combined"],
	)
}

// CapturedToday return whether the lecture were captured today.
func (l *Lecture) CapturedToday() bool {
	return l.Tidspunkt.Year() == time.Now().Year() && l.Tidspunkt.YearDay() == time.Now().YearDay()
}
