package smal

import (
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"path/filepath"
	"regexp"
	"strconv"

	"github.com/hashicorp/go-getter" // For downloading
)

// Download holds the lecture with desired type
type Download struct {
	Lecture      Lecture
	DownloadType string
}

// Result holds the given result about a download
type Result struct {
	status bool
	err    error
}

// StartDownload all files at urls passed as argument
func StartDownload(config *Config, downloads []Download) []Result {

	// Max number of concurrent download from CONST in smal.go
	maxDownloaders := MaxConcurrentDownloads

	// Find number of downloads to do
	numDownloads := len(downloads)

	// Declare some needed variables
	results := make([]Result, numDownloads)
	jobs := make(chan Download, numDownloads)
	resultsChan := make(chan Result, numDownloads)

	// create downloaders
	for d := 1; d <= maxDownloaders; d++ {
		go downloader(jobs, resultsChan, config)
	}

	fmt.Printf("\nSMAL: Found %v matching lectures. Will attempt at retrieving all.\n\nStarting download...\n\n", numDownloads)

	// assign jobs to downloaders
	for j := 0; j < numDownloads; j++ {
		jobs <- downloads[j]
	}

	close(jobs)

	// Wait for download complete or interupt
	// collect results
	for i := 0; i < numDownloads; i++ {
		results[i] = <-resultsChan
	}

	close(resultsChan)

	return results
}

// Downloader(worker): downloads lectures as long as there are urls on jobs
// channel.
func downloader(jobs <-chan Download, results chan<- Result, config *Config) {
	for j := range jobs {

		// Expand any environment variables if exists in OutDir
		// created temp dir
		dir := os.ExpandEnv(config.OutDir)
		tmpDir, err := ioutil.TempDir(dir, "./.part")

		if err != nil {
			log.Fatal(err)
		}

		// Build the client
		opts := []getter.ClientOption{}
		opts = append(opts, getter.WithProgress(defaultProgressBar))

		// Remove unwanted characters from the subject
		lectRex := regexp.MustCompile(`[/@ ]+`)
		course := lectRex.ReplaceAllLiteralString(j.Lecture.Emne, "-")

		// Remove unwanted chars from lecture title
		//rt := regexp.MustCompile("[/ \\,.?!;:*']+")
		//title := rt.ReplaceAllLiteralString(j.Lecture.Tittel, "-")

		// Format time
		fmtOutTime := j.Lecture.Tidspunkt.Format("2006-01-02_15:04")

		// Construct the destination adresse
		// TOOD: See if this can be implemented with setting
		// mode to file download.  If doing so, we must check
		// first for the file extension from the URL, and then
		// add it back.
		//
		// Initial wish for this is so that only a single folder
		// is used when downloading lectures, and creating a new
		// path for each download.

		// tmp directory for downloading to.
		dst := filepath.Join(tmpDir, course, "/", fmtOutTime)

		client := &getter.Client{
			Src:     j.Lecture.URL[j.DownloadType],
			Dst:     dst,
			Mode:    getter.ClientModeAny,
			Options: opts,
		}

		// Start download
		if err := client.Get(); err != nil {
			results <- Result{status: false, err: err}
			os.RemoveAll(tmpDir) // clean up
		} else {
			// Download complete, rename and move file.

			// Read files in download dir
			files, err := ioutil.ReadDir(dst)
			if err != nil {
				log.Fatal(err)
			}

			// Get year of lecture
			year := strconv.Itoa(j.Lecture.Tidspunkt.Year())

			// TODO: To make costume file directory patterns,
			// 	 input them below
			pathNew := filepath.Join(dir, course, year)

			// Make new permanent directory for files.
			err = os.MkdirAll(pathNew, 0700)
			if err != nil {
				log.Fatal(err)
			}

			// For each file downloaded(normaly only one) in
			// current dst, move and rename to "outdir"
			for _, file := range files {

				// TODO: To make costume file name,patterns
				// input them below

				// Rename(move) file according to standard.
				err = os.Rename(filepath.Join(dst, file.Name()),
					pathNew+
						fmt.Sprintf("/%v-%v-%v%v",
							course,
							fmtOutTime,
							j.DownloadType,
							filepath.Ext(file.Name())))
				if err != nil {
					log.Fatal(err)
				}
			}
			results <- Result{status: true, err: nil}
			os.RemoveAll(tmpDir) // clean up tmp dir
		}
	}
}
