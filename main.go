package main

import (
	"gitlab.com/hitchhikers/scrape-me-a-lecture/cmd"
)

func main() {
	// Moving scrape under a command.
	cmd.Execute()
}
