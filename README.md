# Scrape Me A Lecture

SMAL is a small _(no pun intended)_ utility for downloading new lectures as they are released on the [website](https://forelesning.gjovik.ntnu.no).

The tool is written in Go, and is using the Colly framework for scraping the site.  It then uses go-getter for downloading said files.

## Setup Environment

Below is some general steps to get you started contributing.

```sh
export GOPATH=$HOME/.go
export PATH=$PATH:$GOPATH/bin
```

Add repo as a private, or else you'll recieve an error.

```sh
go env -w GOPRIVATE=gitlab.com/hitchhikers/scrape-me-a-lecture
```

Read more about Go modules [here](https://github.com/golang/go/wiki/Modules).

Alternatively:

```gitconfig
[url "git@gitlab.com:"]
  insteadOf = https://gitlab.com/
```

Might need the above because it's a private repo.

## Run the app

```go
go run .
```

## Install (DON'T USE)

```go
go get -u gitlab.com/hitchhikers/scrape-me-a-lecture
```

## Deubg

If all else fails:

```sh
go clean -cache -modcache -i -r
```

Use only for removing the cache.


## Resources

* [Forelesninger](https://forelesning.gjovik.ntnu.no)
* [Colly](http://go-colly.org)
* [go-getter](https://github.com/hashicorp/go-getter)

## Pre-commit-hooks

Install pre-commit from [https://pre-commit.com/](https://pre-commit.com/)

Run command below in repo-folder to install hook:

```sh
pre-commit install
```

Then the checks in `.pre-commit-config.yaml` will be run before each commit.
