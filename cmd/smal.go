package cmd

import (
	"fmt"
	"log"
	"os"

	"github.com/urfave/cli/v2"
	"gitlab.com/hitchhikers/scrape-me-a-lecture/smal"
)

var smalConfig = &smal.Config{}

var configPathFromArgument *string = new(string)

var rootCmdDesc = `
	SMAL (scrape-me-a-lecture is a utility writting to help out with
	scraping lectures from the website of NTNU in Gjøvik.  Using either
	the terminal or a config file, the scraper can be fully automated.`

var app = &cli.App{
	Name:        "smal",
	Usage:       "A small util for scraping leactures from NTNU in Gjøvik",
	Description: rootCmdDesc,
	Action: func(c *cli.Context) error {
		cli.ShowAppHelp(c)

		return nil
	},
	Authors: []*cli.Author{
		&cli.Author{
			Name:  "MindTooth",
			Email: "contact@mindtooth.no",
		},
	},
	Flags: []cli.Flag{
		&cli.StringFlag{
			Name:        "config",
			Usage:       "Load configuration from `FILE`",
			Destination: configPathFromArgument,
		},
		&cli.BoolFlag{
			Name:        "verbose",
			Usage:       "Enable verbose output",
			Destination: &smal.VerboseEnabled,
		},
	},
	Copyright: smal.SmalAuthor,
	Version:   smal.Version,
}

// Execute adds all child commands to the root command and sets flags appropriately.
// This is called by main.main(). It only needs to happen once to the rootCmd.
func Execute() {
	err := initConfig()
	if err != nil {
		fmt.Println(err)
		os.Exit(2)
	}

	app.Commands = []*cli.Command{
		scrapeCmd,
	}

	if err := app.Run(os.Args); err != nil {
		log.Fatal(err)
		os.Exit(1)
	}

}

func initConfig() (err error) {
	var configPath string

	if *configPathFromArgument == "" {
		configPath, err = smal.FindConfig(smal.ConfigLocations)
		if err != nil {
			fmt.Println("Can't find configs:", err)
			return err
		}
	} else {
		configPath = *configPathFromArgument
	}

	err = smalConfig.ReadConfig(configPath)
	if err != nil {
		fmt.Println("Could not find ", configPath, err)
		return err
	}

	return nil
}
