package cmd

import (
	"errors"
	"strings"
	"time"

	"github.com/urfave/cli/v2"
	"gitlab.com/hitchhikers/scrape-me-a-lecture/smal"
)

var scrapedLectures = &[][]smal.Lecture{}
var sinceDate *string = new(string)
var earliestDate time.Time

var scrapeCmd = &cli.Command{
	Name:                   "scrape",
	Aliases:                []string{"s"},
	Usage:                  "Scraping of lectures",
	Description:            "Use various flags when scraping.",
	UseShortOptionHandling: true,
	Flags: []cli.Flag{
		&cli.UintFlag{
			Name:    "count",
			Aliases: []string{"c"},
			Value:   5,
			Usage:   "Set count for many downloads",
		},
		&cli.BoolFlag{
			Name:    "download",
			Aliases: []string{"d"},
			Usage:   "Download lectures",
		},
		&cli.StringFlag{
			Name:        "since",
			Aliases:     []string{"s"},
			Usage:       "Select which capture type",
			Destination: sinceDate,
		},
		&cli.BoolFlag{
			Name:    "today",
			Aliases: []string{"t"},
			Usage:   "Get todays captures lectures",
		},
		&cli.StringFlag{
			Name:    "type",
			Aliases: []string{"T"},
			Value:   "screen",
			Usage:   "Select which capture type",
		},
	},
	Action: scrapeAction,
}

var scrapeAction = func(c *cli.Context) error {
	var err error

	if c.String("since") != "" {
		earliestDate, err = time.Parse("2006-01-02", *sinceDate)
		if err != nil {
			panic(err)
		}
	} else {
		earliestDate, _ = time.Parse("2006-01-02", time.Now().Format("2006-01-02"))
	}

	// TODO: Activate with a flag
	initScrape()

	// if c.Bool("today") && len(*scrapedLectures) > 0 {
	// 	scrapeTodayDownload(c)
	// }

	if len(*scrapedLectures) > 0 {
		scrapeTodayDownload(c)
	}

	return nil
}

func initScrape() {
	scrapedLecturesTest, err := smal.CollectLectures(smalConfig)
	if err != nil {
		panic(err)
	}

	scrapedLectures = &scrapedLecturesTest
}

var scrapeTodayDownload = func(c *cli.Context) error {
	downloads := make([]smal.Download, 0)

	for _, query := range *scrapedLectures {
	out:
		for _, lecture := range query {
			if lecture.Tidspunkt.After(earliestDate) {
				downloads = append(downloads, smal.Download{Lecture: lecture, DownloadType: strings.Title(c.String("type"))})
			} else {
				break out
			}
		}
	}

	if len(downloads) <= 0 {
		return errors.New("no lectures to download")
	}

	if c.Bool("download") && len(downloads) > 0 {
		smal.StartDownload(smalConfig, downloads)
	}

	return nil
}
