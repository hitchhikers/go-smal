package main

import (
	"fmt"
	"regexp"
)

func main() {
	text := "1578474122-8a85658c7129/combined.mp4"

	fmt.Println(stripTrailingSlash(text))
	// pat := regexp.MustCompile(`^[^\/]+`)

	// fmt.Println(pat.FindAllString(text, -1))
}

func stripTrailingSlash(str string) []string {
	pattern := regexp.MustCompile(`^[^\/]+`)

	return pattern.FindAllString(str, -1)
}
