package main

import (
	"log"
	"reflect"

	"github.com/pelletier/go-toml"
)

// Config holds the main config
type Config struct {
	Name     string
	Products []Product `toml:"products"`
	Solen    Solen     `toml:"solen"`
}

// Solen does nothing
type Solen struct {
	Text string `toml:"text"`
}

// Product does the same
type Product struct {
	Name    string `toml:"name"`
	Skunt64 `toml:"sku"`
}

func main() {

	file := []byte(`
Name = "Eirik"

[solen]
text = "Hello, world!"

[[products]]
name = "Hammer"
sku = 738594937

[[products]]
name = "Nail"
sku = 284758393
`)

	toml2, _ := toml.LoadBytes(file)

	log.Println(toml2.ToMap())
	log.Println(toml2.Keys())
	log.Println(reflect.TypeOf(toml2.ToMap()))

	config := Config{}

	err := toml.Unmarshal(file, &config)
	if err != nil {
		panic(err)
	}

	log.Println(reflect.TypeOf(config))
	log.Println(config)
}
