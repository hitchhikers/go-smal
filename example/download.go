package main

import (
	"fmt"

	//"gitlab.com/hitchhikers/scrape-me-a-lecture/cmd"
	"gitlab.com/hitchhikers/scrape-me-a-lecture/smal"
)

func main() {

	// Default configuration settings
	configPath, err := smal.FindConfig(smal.ConfigLocations)
	if err != nil {
		fmt.Println("can't find configs:", err)
	}

	config, err := smal.InitConfig(configPath)
	if err != nil {
		fmt.Println("Could not find ", configPath, err)
	}

	fmt.Println(smal.Banner)
	fmt.Println("Version: " + smal.Version)

	// Create a collector for each subject and collect lectures from subject.
	// slice of all lectures from each collector added to map with subject
	// as key and slice of Lectures as value.
	c := smal.NewCollector(config)
	lecturesBySubject := make(map[string][]smal.Lecture, len(config.GetSubject()))
	for _, v := range c {
		lectures, err := v.GetLectures(config)

		if err != nil {
			panic(err)
		}
		lecturesBySubject[v.Subject] = lectures
	}

	//cmd.Execute()

	// DEBUG
	//	if config.GetDebug() {
	//		if len(lecturesBySubject) > 0 {
	//			lecturesBySubject[config.GetSubject()[0]][0].Disp()
	//			for _, v := range lecturesBySubject {
	//				for _, lec := range v {
	//					fmt.Printf("%s %s %s\n",
	//						lec.Emne,
	//						lec.Foreleser,
	//						lec.Tittel,
	//					)
	//				}
	//			}
	//		}
	//	}

	// Test download

	//fmt.Printf("%v\n", lecturesBySubject["SMF2251"][1])

	down := []smal.Download{}
	down = append(down, smal.Download{Lecture: lecturesBySubject["IMT2282"][0], DownloadType: "Screen"})
	down = append(down, smal.Download{Lecture: lecturesBySubject["IMT2282"][1], DownloadType: "Screen"})
	down = append(down, smal.Download{Lecture: lecturesBySubject["IMT2282"][2], DownloadType: "Screen"})
	down = append(down, smal.Download{Lecture: lecturesBySubject["SMF2251"][1], DownloadType: "Screen"})

	smal.StartDownload(down)
}
